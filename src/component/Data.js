import firebase from '../firebase';
import {useState} from 'react';
const Data = ({info}) => {
    const Annuler=()=>{
        const database=firebase.database().ref('Pause').child(info.id);
        database.remove();
    }
    const [data,setData]=new useState([info]);
    const Dépasser=()=>{
        setData([...data.filter(e=>e.id!=info.id),data.map(d=>d.id===info.id)]);
        const database=firebase.database().ref('Pause');
        database.push(data);
    }
    return (
        <div className='task'>
            <ul>
                <li>{info.Users.FirstName}  {info.Users.LastName}  {info.Etat}  </li>
                 <button className='btn' onClick={Annuler} >Annuler</button>
                 <button className='btn' onClick={Dépasser}>Dépasser</button>
                 <button className='btn'>Démarer</button>
            </ul>
        </div>
    )
}

export default Data
