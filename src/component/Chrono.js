import {useState,useEffect,useRef} from 'react';
const Chrono = ({info}) => {
    const [timer,setTimer]=new useState(900);
    const countRef=useRef(0);
    useEffect(()=>{
        countRef.current=setInterval(()=>{
          setTimer(timer=>timer-1)
        },1000)
    },[])
    const formatTime=()=>{
        const getSeconds = `0${(timer % 60)}`.slice(-2);
        const minutes = `${Math.floor(timer / 60)}`;
        const getMinutes = `0${minutes % 60}`.slice(-2);
        return  getMinutes+ `:`+getSeconds 
    }
    return (
        <div>
          {info.Users.FirstName} {info.Users.LastName} en pause depuis :{formatTime()}
        </div>
    )
}

export default Chrono
