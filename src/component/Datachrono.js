import firebase from '../firebase';
import {useState,useEffect} from 'react';
import Chrono from './Chrono';
const Datachrono = () => {
    const[data,setData]=new useState();
    useEffect(()=>{
        const database=firebase.database().ref('Pause');
        database.on('value',(snapshot)=>{
            const data=snapshot.val();
            const datalist=[];
            for(let id in data)
            {
                datalist.push({id,...data[id]});
            }
            setData(datalist);
        })
    })
    return (
        <div>
            {data ? data.map((Data,index)=>(<Chrono info={Data} key={index} />)):''}
        </div>
    )
}

export default Datachrono
