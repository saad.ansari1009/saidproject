import logo from './logo.svg';
import './App.css';
import DataList from './component/DataList';
import Header from './component/Header';
import Datachrono from './component/Datachrono';
function App() {
  return (
    <div className="container">
      <Header/>
      <DataList/>
      <Datachrono/>
    </div>
  );
}

export default App;
